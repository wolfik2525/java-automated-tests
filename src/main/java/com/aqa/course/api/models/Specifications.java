package com.aqa.course.api.models;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;

/**
 * Specifications contain Rest Assured specifications for basic API requests and responses.
 *
 * @author alexpshe
 * @version 1.0
 */
public class Specifications {
    public io.restassured.specification.RequestSpecification baseRequestSpec() {
        RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();
        requestSpecBuilder.setContentType(ContentType.JSON);
        requestSpecBuilder.setBaseUri("http://188.243.214.3:8081");
        return requestSpecBuilder.build();
    }
}
